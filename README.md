# Listagem e Cadastro
> Listagem de estoque, produtos e funcionários

* Exercício 2 - Listagem de produto com verificação e aviso de estoque
* Exercício 3 - Listagem de funcionários e salários, com disponibilidade de reajuste de salário
* Exercício 4 - Listagem de funcionários e salários com a verificação dos 5 maiores salários

## Instalação / Modo de uso

Todos os exercícios foram criados em linguagem C. Para executá-los no SO Windows, basta executar o arquivo com extensão .exe.
Para executá-los no SO Ubuntu (Linux) siga os passos abaixo:

```fazer download da IDE geany: https://www.geany.org/
Após o download, selecionar o script com extensão .cpp dentro de cada pasta dos exercícios e abri-los com a IDE geany
Com os scripts abertos no geany, vá na opção "Construir" >> "build" >> "Compile" >> "Execute"
Após selecionar execute no script aberto, o programa irá executar no terminal

OBS: Caso dê erro ao abrir o terminal, abra um outro terminal antes de executar os comandos para rodar o script 

```

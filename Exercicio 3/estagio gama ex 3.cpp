#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#define MAX 30
#define TOTAL 1000000

struct dados_dos_funcionarios {
	float salario[TOTAL]; //armazena os dados do salario
	char nome[TOTAL][MAX]; //armazena os dados do funcionario
};
struct dados_dos_funcionarios func;


int main() {
	
	setlocale(LC_ALL,"portuguese");
	int i, j; //variaveis de defini�ao da quantidade de funcionarios
	int aux=0, aux2=0; //auxiliares de repeticao
	int x, y; //variaveis de comando
	printf ("Defina a quantidade de funcion�rios da empresa: ");
	scanf ("%d", &j);
	
	for (i=0; i<j; i++) {
		printf ("\nDigite o nome do funcion�rio %d: ", i);
		scanf ("%s",func.nome[i]);
		printf ("Digite o sal�rio de %s: ", func.nome[i]);
		scanf("%f", &func.salario[i]);
	}
	system("CLS");
	
	while (aux==0) {
		printf ("Lista de sal�rios atual:\n");
		for (i=0; i<j; i++) {
			printf ("%d - %s: R$ %.2f\n", i, func.nome[i], func.salario[i]);
		}
		aux2=0;
		while (aux2==0) {
			printf ("\n\nGostaria de fazer um reajuste do sal�rio de um funcion�rio? Digite (1-Sim / 2-N�o): ");
			scanf("%d", &x);
			if (x==1) {
				printf ("Digite o numero do funcion�rio: ");
				scanf ("%d", &y);
				if (y>=j || y<0) {
					printf ("Erro\n");
				}
				else {
					printf ("Digite o novo sal�rio de %s: ", func.nome[y]);
					scanf ("%f", &func.salario[y]);
					printf ("O sal�rio foi ajustado\n\n\n");
					aux2++;
				}
			}
			else if (x==2) {
				aux++;
				aux2++;
			}
			else {
				printf("Comando Inv�lido\n\n");
			}
		}
	}
	
	system("CLS");
	printf ("Lista de sal�rios atualizada:\n");
	for (i=0; i<j; i++) {
			printf ("%d - %s: R$ %.2f\n", i, func.nome[i], func.salario[i]);
		}	
		
}


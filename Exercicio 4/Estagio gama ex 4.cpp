#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 25

int main () {
	int i, j; 
	float maior1, maior2, maior3, maior4, maior5;
	char maior1f[MAX], maior2f[MAX], maior3f[MAX], maior4f[MAX], maior5f[MAX];
	printf ("Defina a quantidade de funcionarios da empresa: ");
	scanf ("%d", &j);
	
	float salario;
	char nome[MAX];
	
	for (i=0; i<j; i++) {
		printf ("Qual e o nome do funcionario %d:", i+1);
		scanf ("%s", nome);
		printf ("Quanto ganha %s: ", nome);
		scanf ("%f", &salario);
		
		if (i==0) {
			maior1 = salario;
			strcpy (maior1f, nome);
		}
		else if (i==1) {
			maior2 = salario;
			strcpy (maior2f, nome);
		}
		else if (i==2) {
			maior3 = salario;
			strcpy (maior3f, nome);
		}
		else if (i==3) {
			maior4 = salario;
			strcpy (maior4f, nome);
		}
		else if (i==4) {
			maior5 = salario;
			strcpy (maior5f, nome);
		}
		else {
			
			if (maior1<salario && maior1<=maior2 && maior1<=maior3 && maior1<=maior4 && maior1<=maior5) {
				maior1 = salario;
				strcpy (maior1f, nome);
			}
			else if (maior2<salario && maior2<=maior1 && maior2<=maior3 && maior2<=maior4 && maior2<=maior5) {
				maior2 = salario;
				strcpy (maior2f, nome);
			}
			else if (maior3<salario && maior3<=maior1 && maior3<=maior2 && maior3<=maior4 && maior3<=maior5) {
				maior3 = salario;
				strcpy (maior3f, nome);
			}
			else if (maior4<salario && maior4<=maior1 && maior4<=maior3 && maior4<=maior2 && maior4<=maior5) {
				maior4 = salario;
				strcpy (maior4f, nome);
			}
			else if (maior5<salario && maior5<=maior1 && maior5<=maior3 && maior5<=maior4 && maior5<=maior4) {
				maior5 = salario;
				strcpy (maior5f, nome);
			}
		}
		printf ("\n");
	}
	
	if (j<=5) {
		printf ("Salarios:\n");
		if (j==1) {
			printf ("%s: R$ %.2f\n", maior1f, maior1);
		}
		else if (j==2) {
			printf ("%s: R$ %.2f\n", maior1f, maior1);
			printf ("%s: R$ %.2f\n", maior2f, maior2);
		}
		else if (j==3) {
			printf ("%s: R$ %.2f\n", maior1f, maior1);
			printf ("%s: R$ %.2f\n", maior2f, maior2);
			printf ("%s: R$ %.2f\n", maior3f, maior3);
			
		}
		else if (j==4) {
			printf ("%s: R$ %.2f\n", maior1f, maior1);
			printf ("%s: R$ %.2f\n", maior2f, maior2);
			printf ("%s: R$ %.2f\n", maior3f, maior3);
			printf ("%s: R$ %.2f\n", maior4f, maior4);
		}
		else if (j==5) {
			printf ("%s: R$ %.2f\n", maior1f, maior1);
			printf ("%s: R$ %.2f\n", maior2f, maior2);
			printf ("%s: R$ %.2f\n", maior3f, maior3);
			printf ("%s: R$ %.2f\n", maior4f, maior4);
			printf ("%s: R$ %.2f\n", maior5f, maior5);
		}
	}
	else {
	printf ("5 Maiores salarios:\n");
	printf ("%s: R$ %.2f\n", maior1f, maior1);
	printf ("%s: R$ %.2f\n", maior2f, maior2);
	printf ("%s: R$ %.2f\n", maior3f, maior3);
	printf ("%s: R$ %.2f\n", maior4f, maior4);
	printf ("%s: R$ %.2f\n", maior5f, maior5);
	}
}
